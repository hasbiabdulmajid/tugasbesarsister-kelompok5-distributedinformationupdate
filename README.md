Tugas Besar Sister : Distributed Information Update Menggunakan Indirect Communication (Pub Sub)
======

## Anggota Kelompok 5 :
1. Deby Nofalia
2. Hela Irmawati
3. Rachman Noor Sidiq
4. Muhammad Hasbi A.M

## Deskripsi Tugas
Merupakan sebuah program stock market trading (jual - beli) menggunakan metode Indirect Communication dengan arsitektur Publisher-Subscriber yang mana memberlakukan penjual sebagai publisher dan pembeli sebagai subscriber. Pada tugas ini terdapat 2 publisher dengan label `market1` dan `market2` dan 1 subscriber. Program ini dibuat menggunakan bahasa pemrograman Python.

## Arsitektur
![Arsitektur_Tubes_Sister](/uploads/c9b8f4ae6dbd788123293ab454dd70b1/Arsitektur_Tubes_Sister.png)

### Broker Address
Broker berfungsi sebagai penerus informasi stock market dari publisher ke subscriber, broker memiliki nama `localhost` dengan port `1883`.

### Subscriber 
Subscriber pada program ini berlangganan pada topik `market1` dan `market2`. Pada subscriber terdapat sebuah variabel `temp` untuk menampung isi dari payload ketika menerima pesan dari topik yang sudah di subscribe dan terdapat juga fungsi `cetak(x)` yang berfungsi untuk menanyakan kepada subscriber apakah ingin membeli suatu stock atau tidak dengan menginputkan `(1/0)`, jika berhasil membeli maka akan keluar output "Selamat anda berhasil membeli stock" sedangkan jika tidak maka "Stock Tidak terbeli"

### Publisher 1
Publisher 1 mempublish dengan topik `market1`. Publisher akan dimintai input untuk memasukan harga market, setelah selesai input harga maka hasil inputan akan dipublish ke topic, dan akan diteruskan broker kepada subscriber yang sudah berlangganan. Harga yang diinputkan hanya berupa integer (tanpa tanda titik dan koma).

### Publisher 2
Publisher 2 mempublish dengan topik `market2`. Publisher akan dimintai input untuk memasukan harga market setelah selesai input harga maka hasil inputan akan dipublish ke topic, dan akan diteruskan broker kepada subscriber yang sudah berlangganan. Harga yang diinputkan hanya berupa integer (tanpa tanda titik dan koma).


## Fitur Tambahan Program (Coding On The Spot)

### Fitur Tambahan 1 : Hela Irmawati

####PENJELASAN TEKNIS FITUR 1 : History Pembelian
Algoritma Bagian 1 :
```
history_pembelian= []
```
Penjelasan algoritma 1 : <br/>
Inisialisasi global variabel list history pembelian
Algoritma Bagian 2 :
```
def beli(x) :
	history_pembelian.append(x)
```
Penjelasan algoritma 2 : <br/>
Fungsi akan dipanggil di program utama, fungsi ini berfungsi untuk menambahkan harga yang terbeli ke dalam list histoory pembelian
### Fitur Tambahan 2 : Deby Nofalia

#### PENJELASAN TEKNIS FITUR 2 : Export History ke file txt
Algoritma program : <br/>
```
def eksport_beli(x) :
	x = np.savetxt('history.txt', (x))
```

Penjelasan algoritma : <br/>
Dengan menggunakan library numpy untuk save txt, maka dapat mencatat secara berkala history pembelian ke dalam file history.txt.
`def eksport_beli(x)` juga merupakan fungsi yang dipanggil di program utama.

### Fitur Tambahan 3 : Muh Hasbi AM

#### PENJELASAN TEKNIS FITUR 3 : Saldo Pembelian
Algoritma Bagian 1 :
```
saldo_awal = input("Masukan Jumlah Saldo Awal : ")
saldo = int(saldo_awal)
saldolist = []
saldolist.append(saldo)
```
Penjelasan ALgoritma Bagian 1 : <br/>
Algoritma tersebut berfungsi untuk inisialisasi variabel yang nanti akan diolah, `saldo` merupakan variabel untuk mengkonversi nilai inputan ke tipe data string dimana isi dari variabel `saldo` akan di masukkan ke dalam `saldolist` agar bisa diproses di main program
Algoritma Bagian 2 :
```
    saldo_tmp = saldolist[-1]
    print("Jumlah Saldo ", saldo_tmp)
    print("Pesan diterima",str(message.payload.decode("utf-8")))
    harga = int(message.payload.decode("utf-8"))
    temp = harga
    print("harga market : ",temp)
    cetak(temp,saldo_tmp)
```
Penjelasan algoritma bagian 2 : <br/>
pada bagian ini terdapat sebuah variabel `saldo_tmp` untuk menampung isi terakhir dari list `saldolist` yang mana merupakan isi saldo saat ini. lalu variabel `saldo_tmp` ini akan diproses ketika fungsi `cetak(temp,saldo_temp)` dipanggil.

Algoritma Bagian 3 :
```
def cetak(x,z):
	print("Apakah anda ingin membeli nya (1/0)? ")
	jawab = input()
	if (jawab == 1) :
		c = z
		if (c >= x) :
			beli(x)
			
			y = history_pembelian[-1] #menampung isi terakhir dr list history
		
			c -= y
		
			saldolist.append(c)
			print("Selamat anda berhasil membeli, Sisa saldo : ",c)
		else :
			print("Maaf saldo anda tidak mencukupi!, Sisa saldo : ",c)
```
Penjelasan algoritma bagian 3 : <br/>
Pada fungsi cetak nilai dari variabel `saldo_temp` akan dipindah ke variabel `c`, dimana nanti akan digunakan untuk membandingkan harga yang didapat dari subscriber dengan jumlah saldo saat ini, jika saldo mencukupi untuk membeli maka akan ada variabel `y` yang menampung hasil terakhir history pembelian yang digunakan untuk mengurangi nilai saldo saat ini. sehingga `c -= y` setelah nilai c dikurangi maka nilai saldo akan diupdate dengan memasukkan nilai `c` ke dalam `saldolist` sehingga nanti ketika saldo_temp pada program bagian 2 ditampilkan lagi nilai saldo sudah terupdate
### Fitur Tambahan 4 : Rachman NS

#### PENJELASAN TEKNIS FITUR 4 : Isi ulang saldo


Algoritma Fitur : 
```
print("Maaf saldo anda tidak mencukupi!, Sisa saldo : ",c)
print("Ingin isi ulang saldo? 1/0")
jawab2 = input()
if(jawab2 == 1) :
    inp = input("Input Tambahan Saldo :")
	isi = int(inp)
	c += isi
	saldolist.append(c)
	print("Selamat isi ulang berhasil, Sisa saldo : ",c)
```


Penjelasan Algoritma : <br/>
`print("Maaf saldo anda tidak mencukupi!, Sisa saldo : ",c)`<br/>
`print("Ingin isi ulang saldo? 1/0")`

Pada saat publisher mempublish suatu harga saham, subscriber yg subscribe pada topik tersebut, akan diberi tawaran untuk membeli saham tersebut. Apabila ternyata saldo dari subscriber tersebut tidak mencukupi dari harga saham yg ditawarkan. Maka dengan algoritma di atas, program akan memunculkan tulisan yang menyatakan bahwa saldo subscriber tidak mencukupi, dan menanyakan apakah subscriber ingin melakukan isi ulang saldo.


`jawab2 = input()` <br/>
Input jawaban dari subscriber akan disimpan di variabel `jawab2`. 
Subscriber menginput `1` apabila ingin mengisi ulang, dan menginput `0` apabila tidak ingin mengisi ulang saldo.

```
if(jawab2 == 1) :
	inp = input("Input Tambahan Saldo :")
	isi = int(inp)
```
Jika jawaban yang tersimpan pada variable `jawab2` merupakan `1`, yang berarti subscriber menyatakan ingin isi ulang. Maka program akan meminta subscriber untuk memasukkan berapa saldo yang ingin ditambahkan, kemudian akan disimpan di dalam variabel `inp`. 
Namun input yg tersimpan memiliki tipe data string, sehingga tidak bisa diproses untuk kebutuhan penghitungan saldo dalam program. Oleh karena itu input yg tersimpan dalam variable `inp`, diubah menjadi tipe data integer sehingga dapat dilakukan perhitungan tambah/kurang saldo, dan disimpan ke dalam variable `isi`.
```
c += isi 
	saldolist.append(c)
	print("Selamat isi ulang berhasil, Sisa saldo : ",c)
```
Maka variable `c` yang menampung data saldo saat ini, akan ditambahkan dengan data yang tertampung dalam variable `isi`. Kemudian `saldolist.append` digunakan untuk mengupdate data saldo. Dan yang terakhir, maka program akan menampilkan tulisan yang menyatakan bahwa subscriber telah berhasil melakukan isi ulang saldo.