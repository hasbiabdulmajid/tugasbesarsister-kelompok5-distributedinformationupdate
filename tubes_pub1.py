# import paho mqtt
import paho.mqtt.client as mqtt

# import time untuk sleep()
import time

# import datetime untuk mendapatkan waktu dan tanggal
import datetime
#now = datetime.datetime.now()


# definisikan nama broker yang akan digunakan
broker_address = "localhost"

# buat client baru bernama P2
print("creating new instance")
client = mqtt.Client("P2")

# client.on_publish=on_publish

# koneksi ke broker
print("connecting to broker")
client.connect(broker_address,port=1883)


# mulai loop client
client.loop_start()


# lakukan 20x publish waktu dengan topik 1
print("publish something")
print("Masukan Harga : ")
harga = input()
client.publish("market1",payload=harga)
hari = 1
i = 0

print("simulasi selesai")


#stop loop
client.loop_stop()

