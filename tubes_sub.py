# import paho mqtt
import paho.mqtt.client as mqtt

import numpy as np

# import time for sleep()
import time

temp = int()
history_pembelian= []
saldo_awal = input("Masukan Jumlah Saldo Awal : ")
saldo = int(saldo_awal)
saldolist = []
saldolist.append(saldo)
# buat callback on_message; jika ada pesan
# maka fungsi ini akan dipanggil secara asynch
########################################
def on_message(client, userdata, message):
    saldo_tmp = saldolist[-1]
    print("Jumlah Saldo ", saldo_tmp)
    print("Pesan diterima",str(message.payload.decode("utf-8")))
    harga = int(message.payload.decode("utf-8"))
    temp = harga
    print("harga market : ",temp)
    cetak(temp,saldo_tmp)
    print("histori : ")
    print(history_pembelian)
    eksport_beli(history_pembelian)
    
    
    
def cetak(x,z):
	print("Apakah anda ingin membeli nya (1/0)? ")
	jawab = input()
	if (jawab == 1) :
		c = z
		if (c >= x) :
			beli(x)
			
			y = history_pembelian[-1] #menampung isi terakhir dr list saldo
		
			c -= y
		
			saldolist.append(c)
			print("Selamat anda berhasil membeli, Sisa saldo : ",c)
		else :
			print("Maaf saldo anda tidak mencukupi!, Sisa saldo : ",c)
			print("Ingin isi ulang saldo? 1/0")
			jawab2 = input()
			if(jawab2 == 1) :
				inp = input("Input Tambahan Saldo :")
				isi = int(inp)
				
				c += isi 
				saldolist.append(c)
				print("Selamat isi ulang berhasil, Sisa saldo : ",c)
	else:
		print("Tidak terbeli")
	
def beli(x) :
	history_pembelian.append(x)


def eksport_beli(x) :
	x = np.savetxt('history.txt', (x)) 
########################################
    
# buat definisi nama broker yang akan digunakan
broker_address = "localhost"

# buat client baru bernama P1
print("creating new instance")
client = mqtt.Client("P1")


# kaitkan callback on_message ke client
client.on_message = on_message


# buat koneksi ke broker
print("connecting to broker")
client.connect(broker_address,port = 1883)

# jalankan loop client
client.loop_start()

# client melakukan subsribe ke topik 1
print("Subs to topic ")
client.subscribe("market1")
client.subscribe("market2")


# loop forever
while True:
    # berikan waktu tunggu 1 detik 
	time.sleep(1)
	


#stop loop
client.loop_stop()
